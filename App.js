/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local */
import database from './database.json'
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AgeValidator from './components/AgeValidator';
import MyList from  './components/MyList'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textValue: '',
      database
    };
  }

  changeTextInput = text => {
    let regex = /^-?\d*\.?\d*$/;
    if (regex.test(text)) {
      this.setState({
        textValue: text,
      });
    }
  };

  render() {
    return (
      <View>
        <View style={styles.panel}>
        <Text
          style={{
            textAlign: 'center',
            marginBottom: 20,
            fontSize: 20,
            color: '#4066ff',
            fontWeight: 'bold',
          }}>
          Laboratorio 3 Tarea
        </Text>
        <AgeValidator
          onChangeTextInput={text => this.changeTextInput(text)}
          textValue={this.state.textValue}
        />
        </View>
        <View style={{marginTop:10}} >
          <MyList data={database} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panel: {
    marginHorizontal: 20,
    marginTop: 20,
    padding: 25,
    backgroundColor: '#f0f0f0',
    borderRadius: 20,
    elevation: 5,
  },
});
