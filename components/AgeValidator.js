import React from 'react';
import { StyleSheet, View, Text, TextInput} from 'react-native';

export default class InputTextField extends React.Component {
  constructor(props){
    super(props);
  }
  render(){
    return (
      <View>
        <Text>Edad:</Text>
        <TextInput
          style={styles.input}
          onChangeText={this.props.onChangeTextInput}
          value={this.props.textValue}
        />
        <View style={{ borderBottomColor: "#59b7ff", borderBottomWidth: 1 }} />
        <Text style={{textAlign: "center", fontSize: 15, fontWeight:'bold', marginTop:10}}>
          { this.props.textValue == ''? 'Ingrese su edad' : this.props.textValue > 18 ? 'Es mayor de edad' : 'Es menor de edad'}
        </Text>
      </View>
    )
  }
  
}

const styles = StyleSheet.create({
    inputTitle: {
        color: "#ABB4BD",
        fontSize: 14
    },
    input: {
        paddingVertical: 12,
        fontSize: 14,
        fontFamily: "Avenir Next"
    }
});
