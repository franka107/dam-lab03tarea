import React from 'react'
import {View, ScrollView, Text, Image, SafeAreaView, FlatList, StyleSheet} from 'react-native'

function Item({ title, image, description }) {
  let direction = image.substr(0, image.indexOf(','))
  return (
    <View  style={[styles.panel, {flexDirection: 'row'} ]}>
      <Image style={{width:100, height:100,}} source={{uri: direction|| 'test' }}/>
      <View style={{ paddingRight: 80}}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>{title} </Text>
        <Text>{description} </Text>
      </View>
      
    </View>
  );
}



export default class MyList extends React.Component{
  constructor(props){
    super(props);
  }


  render(){
    return(
      <View>
        <FlatList
          data={this.props.data}
          renderItem={({item}) => 
            <Item  title={item.Nombre} image={item.Imágenes} description={item['Descripción corta']}/>
          }
          keyExtractor={item => item.SKU}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  panel: {
    marginHorizontal: 20,
    marginTop: 20,
    padding: 25,
    backgroundColor: '#f0f0f0',
    borderRadius: 20,
    elevation: 5,
  },
});